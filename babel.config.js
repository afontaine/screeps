module.exports = (api) => {
  const isTest = api.env('test');
  api.cache(true);
  return {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            [isTest ? 'node' : 'esmodules']: true,
          },
        },
      ],
    ],
    include: ['src/', 'node_modules/'],
    exclude: [],
    ignore: [() => false],
  };
};
