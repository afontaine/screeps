import { config } from 'dotenv';

config();

const email = process.env.SCREEPS_EMAIL;
const token = process.env.SCREEPS_TOKEN;

export default {
  // Used for deploying to the main world
  main: {
    email,
    token,
    protocol: 'https',
    hostname: 'screeps.com',
    port: 443,
    path: '/',
    branch: 'auto',
  },
  // Used for deploying to Simulation mode
  sim: {
    email,
    token,
    protocol: 'https',
    hostname: 'screeps.com',
    port: 443,
    path: '/',
    branch: 'sim'
  },
};
