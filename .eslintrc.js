module.exports = {
  env: {
    es6: true,
    node: true,
    'screeps/screeps': true,
  },
  extends: 'airbnb-base',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  plugins: ['screeps'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {},
  overrides: [
    {
      files: ['main.js'],
      rules: {
        'import/prefer-default-export': 'off',
      },
    },
    {
      files: ['*.test.js'],
      plugins: ['jest'],
      env: {
        jest: true,
      },
    },
  ],
};
