import run from './roles';
import roads from './rooms/roads';

export function loop() {
  Object.keys(Memory.creeps)
    .filter(name => !Game.creeps[name])
    .forEach((name) => {
      delete Memory.creeps[name];
    });

  const { creeps, rooms } = Game;
  run(creeps);
  Object.keys(rooms).map(room => rooms[room]).forEach(room => roads(room));
}
