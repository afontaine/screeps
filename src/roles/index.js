import filter from 'lodash-es/filter';

import roles from './roles';

const runRole = ({ role, work, spawn }, creeps) => {
  const workers = filter(creeps, creep => creep.memory.role === role);

  spawn(workers);

  workers.forEach(work);
};

export default (creeps) => {
  roles.forEach(role => runRole(role, creeps));
};
