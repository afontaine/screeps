import * as harvester from './harvester';
import * as upgrader from './upgrader';
import * as builder from './builder';

export default [harvester, upgrader, builder];
