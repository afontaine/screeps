import { UPGRADER as role } from './constants';
import { spawner, nearestSource } from './utils';

export const spawn = spawner({ role, count: 2, parts: [WORK, CARRY, MOVE] });

export const work = (creep) => {
  if (creep.memory.upgrading && creep.carry.energy === 0) {
    Memory.creeps[creep.name].upgrading = false;
    creep.say('🔄 harvest');
  }
  if (!creep.memory.upgrading && creep.carry.energy === creep.carryCapacity) {
    Memory.creeps[creep.name].upgrading = true;
    creep.say('⚡ upgrade');
  }

  if (creep.memory.upgrading) {
    if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
      creep.moveTo(creep.room.controller, {
        visualizePathStyle: { stroke: '#ffffff' },
      });
    }
  } else {
    const source = nearestSource(creep.room.controller);
    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
      creep.moveTo(source, { visualizePathStyle: { stroke: '#ffaa00' } });
    }
  }
};

export { role };
