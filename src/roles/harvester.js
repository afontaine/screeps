import { HARVESTER as role } from './constants';
import { spawner } from './utils';

export const spawn = spawner({ role, count: 3, parts: [WORK, CARRY, MOVE] });

export const work = (creep) => {
  if (creep.carry.energy < creep.carryCapacity) {
    const [source] = creep.room.find(FIND_SOURCES);
    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
      creep.moveTo(source, { visualizePathStyle: { stroke: '#ffaa00' } });
    }
  } else {
    const [target] = creep.room.find(FIND_STRUCTURES, {
      filter(structure) {
        return (
          (structure.structureType === STRUCTURE_EXTENSION
            || structure.structureType === STRUCTURE_SPAWN
            || structure.structureType === STRUCTURE_TOWER)
          && structure.energy < structure.energyCapacity
        );
      },
    });
    if (target) {
      if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
        creep.moveTo(target, { visualizePathStyle: { stroke: '#ffffff' } });
      }
    } else {
      const [site] = creep.room.find(FIND_CONSTRUCTION_SITES);
      if (site) {
        if (creep.build(site) === ERR_NOT_IN_RANGE) {
          creep.moveTo(site, { visualizePathStyle: { stroke: '#ffffff' } });
        }
      } else if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
        creep.moveTo(creep.room.controller, {
          visualizePathStyle: { stroke: '#ffffff' },
        });
      }
    }
  }
};

export { role };
