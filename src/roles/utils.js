const DEFAULT_TEST = () => true;

export const spawner = ({
  role, count, parts, test = DEFAULT_TEST,
}) => (creeps) => {
  console.log(`${role}: ${creeps.length}`);

  if (creeps.length < count && test()) {
    const newName = `${role}${Game.time}`;
    console.log(`Spawning new ${role}: ${newName}`);
    Game.spawns.Spawn1.spawnCreep(parts, newName, {
      memory: { role },
    });
  }
};

export const nearestSource = (structure) => {
  const [{ source }] = structure.room
    .find(FIND_SOURCES)
    .map(src => ({
      source: src,
      path: structure.room.findPath(structure.pos, src.pos, { ignorePaths: true }),
    }))
    .sort((x, y) => x.path.length - y.path.length);
  return source;
};
