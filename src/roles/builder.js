import { BUILDER as role } from './constants';
import { spawner } from './utils';

export const spawn = spawner({ role, count: 3, parts: [WORK, CARRY, MOVE] });

export const work = (creep) => {
  if (creep.memory.building && creep.carry.energy === 0) {
    Memory.creeps[creep.name].building = false;
    creep.say('🔄 harvest');
  }
  if (!creep.memory.building && creep.carry.energy === creep.carryCapacity) {
    Memory.creeps[creep.name].building = true;
    creep.say('🚧 build');
  }

  if (creep.memory.building) {
    const [target] = creep.room.find(FIND_CONSTRUCTION_SITES);
    if (target) {
      if (creep.build(target) === ERR_NOT_IN_RANGE) {
        creep.moveTo(target, { visualizePathStyle: { stroke: '#ffffff' } });
      }
    } else if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
      creep.moveTo(creep.room.controller, {
        visualizePathStyle: { stroke: '#ffffff' },
      });
    }
  } else {
    const [source] = creep.room.find(FIND_SOURCES);
    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
      creep.moveTo(source, { visualizePathStyle: { stroke: '#ffaa00' } });
    }
  }
};

export { role };
