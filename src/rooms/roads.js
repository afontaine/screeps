import flatten from 'lodash-es/flatten';

export default (room) => {
  if (room.memory.roads || !room.controller.my) {
    return;
  }

  const sources = room.find(FIND_SOURCES);
  const spawns = room.find(FIND_MY_SPAWNS);
  const { controller } = room;

  const positions = [
    ...sources.map(source => source.pos),
    ...spawns.map(spawn => spawn.pos),
    controller.pos,
  ];

  const paths = positions
    .map(position => [
      position,
      position.findClosestByPath(positions.filter(pos => pos !== position)),
    ])
    .map(([x, y]) => PathFinder.search(x, y));

  flatten(paths).forEach(position => room.createConstructionSite(position, STRUCTURE_ROAD));

  Memory.rooms[room.name].roads = true;
};
