import clear from 'rollup-plugin-clear';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import screeps from 'rollup-plugin-screeps';

import cfg from './config/screeps';

let config;

const dest = process.env.DEST;
if (dest) {
  config = cfg[dest];
  if (config == null) {
    throw new Error('Invalid upload destination');
  }
}

export default {
  input: 'src/main.js',
  output: {
    file: 'dist/main.js',
    format: 'cjs',
    sourcemap: true,
  },
  plugins: [
    clear({ targets: ['dist'] }),
    resolve(),
    commonjs(),
    babel({ exclude: 'node_modules/**' }),
    screeps({ config, dryRun: config == null }),
  ],
};
